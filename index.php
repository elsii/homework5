<?php

// Пытался использувать пространство имен но получал ошибку
// PHP Fatal error: Uncaught Error: Class 'src\components\DbConnect'

include_once 'vendor/autoload.php';
include_once __DIR__ . '/constants.php';

use Classes\DbConnect;
use Classes\MySqlQuery;

$mySqlQuery = new MySqlQuery(new DbConnect(), DB_USER, DB_PASS);

if($_GET && empty($_GET['id_user']))
{
    $mySqlQuery->makeAction($_GET);
    header('Location: /');
    exit();
}

$personalInfo = [];
if($_GET['id_user'])
    $personalInfo = $mySqlQuery->getUserInfo($_GET['id_user']);

?>

<form action="#">
    <?php if(!$mySqlQuery->isUserTableIseet()): ?>
        <button name="action" value="<?= MySqlQuery::ACTION_CREATE_USER_TABLE; ?>">create table user</button>
    <?php else: ?>
        <h2 style="color: green">Таблица пользователя уже создана</h2>
        <div>
            <label for="name">
                Name
            </label>
            <input type="text" required name="name" placeholder="Please enter your Name">
        </div>
        <div>
            <label for="surname">
                Surname
            </label>
            <input type="text" required name="surname" placeholder="Please enter your Surname">
        </div>
        <div>
            <label for="age">
                Age
            </label>
            <input type="number" required name="age" placeholder="Please choice your Age">
        </div>
        <div>
            <label for="email">
                Email
            </label>
            <input type="text" required name="email" placeholder="Please enter your Email">
        </div>
            <label for="phone">
                Phone
            </label>
            <input type="text" name="phone" placeholder="Please enter your Phone">
        </div>

        <div>
            <button name="action" value="<?= MySqlQuery::ACTION_ADDED_USER; ?>">Added new user</button>
        </div>
    <?php endif; ?>
</form>

<?php if($mySqlQuery->isUserTableIseet()): ?>
    <form action="">
        <table border="1">
            <tr>
                <th></th>
                <th>ID</th>
                <th>NAME</th>
                <th>Surname</th>
                <th>age</th>
                <th>Email</th>
                <th>phone</th>
                <th>more info</th>
            </tr>

            <?php foreach ($mySqlQuery->getAllUsers() as $user): ?>
                <tr>
                    <td>
                        <input type="checkbox" name="removeIds[]" value="<?= $user['id']; ?>">
                    </td>
                    <td><?= $user['id']; ?></td>
                    <td><?= $user['name']; ?></td>
                    <td><?= $user['surname']; ?></td>
                    <td><?= $user['age']; ?></td>
                    <td><?= $user['email']; ?></td>
                    <td><?= $user['phone']; ?></td>
                    <td>
                        <a href="/?id_user=<?= $user['id']; ?>">Personal info</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <div>
            <button name="action" value="<?= MySqlQuery::ACTION_REMOVE_USERS; ?>">Remove users</button>
        </div>
    </form>
<?php endif; ?>

<?php if($personalInfo): ?>
    <h2>Personal Info</h2>
    <div>
        ID: <?= $personalInfo['id']; ?>
    </div>
    <div>
        NAME: <?= $personalInfo['name']; ?>
    </div>
    <div>
        Surname: <?= $personalInfo['surname']; ?>
    </div>
    <div>
        age: <?= $personalInfo['age']; ?>
    </div>
    <div>
        Email: <?= $personalInfo['email']; ?>
    </div>
    <div>
        phone: <?= $personalInfo['phone']; ?>
    </div>
<?php endif; ?>
