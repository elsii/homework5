<?php

namespace Classes;

use PDO;

class MySqlQuery
{

    const ACTION_CREATE_USER_TABLE = 1;
    const ACTION_ADDED_USER = 2;
    const ACTION_REMOVE_USERS = 3;

    public function __construct(DbConnect $pdo, $user, $password)
    {
        $this->pdo = $pdo->connect($user, $password);
    }

    /**
     * Function for create query to db
     *
     * @param $query
     * @return false|\PDOStatement
     */
    protected function makeQuery($query, $setBindParams = [])
    {
        $query = $this->pdo->prepare($query);
        $query->execute();
        return $query->fetchAll();
    }

    public function createTableUser()
    {
        $query = "CREATE TABLE user (
            id INT(6) AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            surname VARCHAR(255) NOT NULL,
            age INT(6) NOT NULL,
            email VARCHAR(255) NOT NULL,
            phone VARCHAR(255) DEFAULT '')";

        $this->makeQuery($query);
    }


    /**
     * Check if table use isset
     *
     * @return bool
     */
    public function isUserTableIseet()
    {
        $result = $this->makeQuery("SHOW TABLES LIKE 'user'");
        if($result)
            return true;
        return false;
    }

    protected function addedUser($get)
    {
//        $query = "INSERT INTO user (name, surname, age, email, phone) value (:name, :surname, :age, :email, :phone)";

        $getKeyLeft = [];
        $getKeyRight = [];
        foreach (array_keys($get) as $item)
        {
            if(empty($get[$item]))
                continue;

            $getKeyLeft[] = $item;
            $getKeyRight[] = ":{$item}";
        }

        $getKeyLeft = implode(', ', $getKeyLeft);
        $getKeyRight = implode(', ', $getKeyRight);

        $query = "INSERT INTO user ({$getKeyLeft}) value ({$getKeyRight})";
        $query = $this->pdo->prepare($query);

        $query->bindParam(":name", $get['name']);
        $query->bindParam(":surname", $get['surname']);
        $query->bindParam(":age", $get['age']);
        $query->bindParam(":email", $get['email']);
        if(!empty($get['phone']))
            $query->bindParam(":phone", $get['phone']);

        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Remove user in table
     *
     * @param $get
     * @return array|false
     */
    protected function removeUser($get)
    {
        $ids = implode(', ', $get['removeIds']);
        $query = "DELETE FROM user WHERE id IN ({$ids});";
        $this->pdo->query($query);
        return true;
    }

    public function makeAction($get)
    {
        if(!isset($get['action']))
            return true;

        $action = intval($get['action']);
        unset($get['action']);
        if($action == self::ACTION_CREATE_USER_TABLE)
            $this->createTableUser();

        if($action == self::ACTION_ADDED_USER)
            $this->addedUser($get);

        if($action == self::ACTION_REMOVE_USERS)
            $this->removeUser($get);

        return true;
    }

    /**
     * Function for get all users
     *
     * @return false|\PDOStatement
     */
    public function getAllUsers()
    {
        $query = 'SELECT * FROM user';
        return $this->makeQuery($query);
    }

    /**
     * Function for get user info
     *
     * @param $id_user
     * @return array|false
     */
    public function getUserInfo($id_user)
    {
        $query = "SELECT * FROM user WHERE id = :id";
        $query = $this->pdo->prepare($query);

        $query->bindParam(":id", $id_user);
        $query->execute();
        $resultArray = $query->fetchAll();
        if(!$resultArray)
            return [];

        return array_shift($resultArray);
    }
}