<?php

namespace Classes;

interface IdbConnect {
    public function connect($user, $password);
}